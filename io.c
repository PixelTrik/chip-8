#include <SDL2/SDL.h>
#include "io.h"

SDL_Window*   window;
SDL_Renderer* renderer;

byte scale = 10;

color fg = {0xeb, 0xdb, 0xb2};
color bg = {0x28, 0x28, 0x28};

bool should_quit = false;

/* Keypad structure
 * | 1 | 2 | 3 | C |
 * | 4 | 5 | 6 | D |
 * | 7 | 8 | 9 | E |
 * | A | 0 | B | F |
 */

SDL_Scancode keymappings[16] = {
  SDL_SCANCODE_X, 
  SDL_SCANCODE_1, SDL_SCANCODE_2, SDL_SCANCODE_3, 
  SDL_SCANCODE_Q, SDL_SCANCODE_W, SDL_SCANCODE_E, 
  SDL_SCANCODE_A, SDL_SCANCODE_S, SDL_SCANCODE_D, 
  SDL_SCANCODE_Z, SDL_SCANCODE_C, SDL_SCANCODE_4, // 0xA, 0xB, 0xC
  SDL_SCANCODE_R,  SDL_SCANCODE_F, SDL_SCANCODE_V // 0xD, 0xE, 0xF
};

void swap(byte* x, byte* y) {
  *x ^= *y;
  *y ^= *x;
  *x ^= *y;
}

void init_io() {
  SDL_Init(SDL_INIT_VIDEO);

  window = SDL_CreateWindow("Chip-8 Emulator", 
      SDL_WINDOWPOS_CENTERED, 
      SDL_WINDOWPOS_CENTERED, 
      64 * scale, 32 * scale, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);

  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

  if (SDL_Init(SDL_INIT_AUDIO) != 0)
    SDL_Log("Failed to initialize SDL: %s", SDL_GetError());
}

void draw(byte* gfx) {
  // Clearing screen
  SDL_SetRenderDrawColor(renderer, bg.red, bg.green, bg.blue, 255);
  SDL_RenderClear(renderer);
  // Preparing for drawing screen
  SDL_SetRenderDrawColor(renderer, fg.red, fg.green, fg.blue, 255);

  for (int y = 0; y < 32; y++) {
    for (int x = 0; x < 64; x++) {
      if (gfx[x + (y * 64)]) {
        SDL_Rect rect;
        rect.x = x * scale;
        rect.y = y * scale;
        rect.w = rect.h = scale;
        SDL_RenderFillRect(renderer, &rect);
      }
    }
  }

  // Provide the output
  SDL_RenderPresent(renderer);
}

void event_handler(byte* keypad) {
  SDL_Event event;

  if (SDL_PollEvent(&event)) {
    const byte* state = SDL_GetKeyboardState(NULL);
    switch(event.type) {
      case SDL_QUIT:
        should_quit = true;
        break;

      case SDL_KEYDOWN:
        if (event.key.keysym.sym == SDLK_SPACE) {
          swap(&fg.red,    &bg.red);
          swap(&fg.blue,   &bg.blue);
          swap(&fg.green,  &bg.green);
        }

      default:
        if (state[SDL_SCANCODE_ESCAPE])
          should_quit = true;
        
        for (int code = 0; code < 16; code++)
          keypad[code] = state[keymappings[code]]; 
    }
  }
}

void audio_callback(void* data, byte* raw_buff, int b_len) {
  double sample_rate  = 44100.0;
  int amplitude       = 28.0;

  short* buff   = (short*) raw_buff;
  int len       = b_len / 2;
  int sample_nr = *(int*) data;

  for (int x = 0; x < len; x++, sample_nr++) {
    double time = sample_nr / sample_rate;
    buff[x]     = (short)(amplitude * sin(2.0f * M_PI * 441.0 * time));
  }
}

void beep() {
  int sample = 0;
  SDL_AudioSpec want, have;

  SDL_zero(want);
  want.freq     = 44100;
  want.format   = AUDIO_F32;
  want.channels = 2;
  want.samples  = 2048;
  want.callback = audio_callback;
  want.userdata = &sample;

  if (SDL_OpenAudio(&want, &have) != 0)
    SDL_LogError(SDL_LOG_CATEGORY_AUDIO, "Failed to open audio: %s", SDL_GetError());

  if (want.format != have.format) 
    SDL_LogError(SDL_LOG_CATEGORY_AUDIO, "Failed to get the desired AudioSpec");

  SDL_PauseAudio(0);
  SDL_Delay(25);
  SDL_PauseAudio(1);
} 

void stop_io() {
  SDL_DestroyWindow(window);
  SDL_CloseAudio();
  SDL_Quit();
}
