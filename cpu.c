#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#include "cpu.h"

// Declaring variables

byte fontset[80] = {
  0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
  0x20, 0x60, 0x20, 0x20, 0x70, // 1
  0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
  0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
  0x90, 0x90, 0xF0, 0x10, 0x10, // 4
  0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
  0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
  0xF0, 0x10, 0x20, 0x40, 0x40, // 7
  0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
  0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
  0xF0, 0x90, 0xF0, 0x90, 0x90, // A
  0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
  0xF0, 0x80, 0x80, 0x80, 0xF0, // C
  0xE0, 0x90, 0x90, 0x90, 0xE0, // D
  0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
  0xF0, 0x80, 0xF0, 0x80, 0x80  // F
};

byte mem[4096]    = {0};
byte V[16]        = {0};
byte gfx[64 * 32] = {0};
byte keys[16]     = {0};
byte delay_timer  = 0;
byte sound_timer  = 0;

word I          = 0;
word pc         = 0x200;
word stack[16]  = {0};
word sp         = 0;

bool draw_flag  = false;
bool sound_flag = false;

void init_cpu() {
  srand((unsigned int) time(NULL));

  // Ensuring proper initialization of arrays.
  memset(gfx,   0x00,   sizeof(gfx));
  memset(mem,   0x00,   sizeof(mem));
  memset(V,     0x00,   sizeof(V));
  memset(keys,  0x00,   sizeof(keys));
  memset(stack, 0x0000, sizeof(stack));

  // Loading font set
  memcpy(mem,   fontset,  80);
}

void load_rom(char* filename) {
  FILE* file = fopen(filename, "rb");

  if (!file) {
    fprintf(stderr, "Invalid filepath: %s", filename);
    exit(1);
  }

  // Get file size
  fseek(file, 0, SEEK_END);
  long file_size = ftell(file);
  rewind(file);

  fread(mem + 0x200, file_size, 1, file);
}

void run() {
  draw_flag   = false;
  sound_flag  = false;

  word opcode = (mem[pc] << 8) | mem[pc + 1];
  word x      = (opcode & 0x0F00) >> 8;
  word y      = (opcode & 0x00F0) >> 4;
  word addr   = (opcode & 0x0FFF);
  word kk     = (opcode & 0x00FF);
  word n      = (opcode & 0x000F);

  switch (opcode & 0xF000) {
    case 0x0000:
      switch (kk) {
        case 0xE0:
          memset(gfx, 0x00, 64 * 32);
          break;

        case 0xEE:
          pc = stack[sp--];
          break;

        default:
          fprintf(stderr, "Unkwnown opcode: 0x%X\n", opcode);
          return;
      }
      pc += 2;
      break;

    case 0x1000:
      pc = addr;
      break;

    case 0x2000:
      stack[++sp] = pc;
      pc = addr;
      break;

    case 0x3000:
      pc += (V[x] == kk) ? 4 : 2;
      break;

    case 0x4000:
      pc += (V[x] != kk) ? 4 : 2;
      break;

    case 0x5000:
      pc += (V[x] == V[y]) ? 4 : 2;
      break;

    case 0x6000:
      V[x] = kk;
      pc += 2;
      break;

    case 0x7000:
      V[x] += kk;
      pc += 2;
      break;

    case 0x8000:
      switch (n) {
        case 0x0:
          V[x] = V[y];
          break;

        case 0x1:
          V[x] |= V[y];
          break;

        case 0x2:
          V[x] &= V[y];
          break;

        case 0x3:
          V[x] ^= V[y];
          break;

        case 0x4:
          V[0xF] = (V[x] + V[y] > 0xFF) ? 1 : 0;
          V[x] += V[y];
          break;

        case 0x5:
          V[0xF] = (V[x] > V[y]) ? 1 : 0;
          V[x] -= V[y];
          break;

        case 0x6:
          V[0xF] = V[x] & 0x01;
          V[x] >>= 1;
          break;

        case 0x7:
          V[0xF] = (V[y] > V[x]) ? 1 : 0;
          V[x] = V[y] - V[x];
          break;

        case 0xe:
          V[0xF] = (V[x] >> 7);
          V[x] <<= 1; 
          break;

        default:
          fprintf(stderr, "Unknown opcode 0x%X", opcode);
          return;
      }
      
      pc += 2;
      break;

    case 0x9000:
      pc += (V[x] != V[y]) ? 4 : 2;
      break;

    case 0xA000:
      I = addr;
      pc += 2;
      break;

    case 0xB000:
      pc = addr + V[0];
      break;

    case 0xC000:
      V[x] = rand() & kk;
      pc += 2;
      break;

    case 0xD000:
      // Drawing Sprite using Dxyn at (V[x], V[y]) with height 'n' pixels
      draw_flag = true;
      
      word height = n;
      word pixel;
      
      V[0xF] = 0;
      for (int y_line = 0; y_line < height; y_line++) {
        pixel = mem[I + y_line];
        
        for (int x_line = 0; x_line < 8; x_line++) {
          if ((pixel & (0x80 >> x_line)) != 0) {
            word pos = (V[x] + x_line) + ((V[y] + y_line) * 64);
            
            V[0xF] = (gfx[pos] == 1) ? 1 : V[0xF];
            gfx[pos] ^= 1;
          }
        }
      }
      
      pc += 2;
      break;

    case 0xE000:
      switch(kk) {
        case 0x9E:
          pc += (keys[V[x]]) ? 4 : 2;
          break;

        case 0XA1:
          pc += (!keys[V[x]]) ? 4 : 2;
          break;
      }
      break;

    case 0xF000:
      switch(kk) {
        case 0x07:
          V[x] = delay_timer;
          break;

        case 0x0A:
          for (int idx = 0; idx < 16; idx++) {
            if (keys[idx]) {
              V[x] = idx;
              pc += 2;
              break;
            }
          }
          // If the key is not pressed then pc will revert to original position.
          // On pressing key, PC will be set 0 which will allow it to move 
          // to other instructions.
          pc -= 2;
          break;

        case 0x15:
          delay_timer = V[x];
          break;

        case 0x18:
          sound_timer = V[x];
          break;

        case 0x1E:
          I += V[x];
          break;

        case 0x29:
          I = V[x] * 5; // Each digit is 40 bits long (5 bytes)
          break;

        case 0x33:
          // Convert V[x] into its Binary Coded Decimal (BCD) equivalent
          mem[I]      = (V[x] / 100);
          mem[I + 1]  = (V[x] % 100) / 10;
          mem[I + 2]  = (V[x] % 10);
          break;

        case 0x55:
          for (int i = 0; i <= x; i++)
            mem[I + i] = V[i];
          break;

        case 0x65:
          for (int i = 0; i <= x; i++)
            V[i] = mem[I + i];
          break;

        default:
          fprintf(stderr, "Unknown opcode 0x%X", opcode);
          return;
      }
      
      pc += 2;
      break;
  }

  // Updating Timers

  if (delay_timer > 0)
    delay_timer--;

  if (sound_timer > 0) {
    sound_flag = true;
    sound_timer--;
  }
}
