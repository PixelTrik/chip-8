#include <stdio.h>
#include <unistd.h>

#include "cpu.h"
#include "io.h"

int main (int argc, char** argv) {
  if (argc != 2) {
    printf("USAGE: chip8 <rom-file>\n");
    return 1;
  }

  char* rom_file = argv[1];

  init_cpu();
  load_rom(rom_file);
  init_io();

  while (true) {
    run();
    event_handler(keys);
    
    if (should_quit)
      break;
    
    if (sound_flag)
      beep();
    
    if (draw_flag)
      draw(gfx);
    
    usleep(1666);
  }

  stop_io();
  return 0;
}
