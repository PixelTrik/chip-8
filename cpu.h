#ifndef CHIP8_CPU
#define CHIP8_CPU

#include <stdbool.h>

// Aliasing data types
typedef unsigned char   byte;
typedef unsigned short  word;

// 16 - bit elements
extern word I;
extern word pc;
extern word stack[16];
extern word sp;

// 8 - bit elements
extern byte fontset[80];
extern byte mem[4096];
extern byte gfx[64 * 32];
extern byte keys[16];
extern byte V[16];
extern byte delay_timer;
extern byte sound_timer;

extern bool draw_flag;
extern bool sound_flag;

void init_cpu();
void load_rom(char*);
void run();
#endif
