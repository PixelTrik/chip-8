CC = gcc
CFLAGS =-O2 -Wall -I/usr/local/include
LDFLAGS =-lm -lSDL2 -L/usr/local/lib

all:
	$(CC) -o chip-8 chip8.c cpu.c io.c $(CFLAGS) $(LDFLAGS)

clean:
	rm -f chip8
