# CHIP-8
A simple CHIP-8 emulator written in C that prioritizes clarity over cleverness.

![IBM Logo](screenshots/ibm-demo.png)

![C8 Logo](screenshots/logo-demo.png)

![Breakout Demo](screenshots/breakout-demo.png)


## How to build this project and play with it?
To build this project simply use (make sure you have `sdl2` installed, that's the only dependency for this project)
```
make
```

And load roms using
```
./chip-8 <rom file path>
```
You can use roms from `roms` folder or any other rom you have.

**Note that this emulator only supports the original CHIP-8 and not the extended versions like Super CHIP-48, etc.**

The keypad for CHIP-8 is
```
| 1 | 2 | 3 | C |
| 4 | 5 | 6 | D |
| 7 | 8 | 9 | E |
| A | 0 | B | F |
```

And the equivalent on PC is
```
| 1 | 2 | 3 | 4 |
| Q | W | E | R |
| A | S | D | F |
| Z | X | C | V |
```

## What is CHIP-8?
CHIP-8 is technically a high level, interpreted programming language made for writing games in micro-computers from 1970s like RCA COSMAC VIP, Telmac 1800, etc. 
This allowed programmers to write more complex games with minimal lines of code since CHIP-8 does the heavy lifting like memory management, routines to clear 
screens, draw sprites, etc. This struck the balance between performance, hardware control and ease of programming by conceptualising CHIP-8 as a virtual machine.

## I've heard CHIP-8 as the "Hello World" of emulators, how true is it?
It is true that CHIP-8 is considered as the quintessential gateway project for emulator developers since it operates on **measly 36 opcodes**, this is low when
compared to **151 opcodes for MOS 6502** that powered NES, Apple II, Commodore VIC-20, Atari 400 and 800, BBC Micro, etc., or **252 opcodes for Zilog Z80** 
that was used in Sinclair computers (Z80, Z81, ZX Spectrum), Microsoft MSX and computers using CP/M like Osborn computers, etc.

Despite being a high level programming language used for DIY micro-computers, the programming was done using hexadecimal codes which is technically binary. 
This made interpreting CHIP-8 no different from writing an emulator. In both cases, you're decoding hexadecimal instructions to drive a program that is made to
behave like another computer architecture.

## What were the specs of CHIP-8?
CHIP-8 was built as a programming language for DIY micro computers, which means there is no actual hardware implementation for CHIP-8. So considering the hardware
specs of those computers back then, the following specs were chosen.

* 4KB RAM
* Monochrome display (2 colors, yay)
* 2 Timers (for sound and delay), counting down at 60Hz.
* 36 opcodes in big-endian format.
* 16 8-bit registers (15 general purpose for the user, 1 for CHIP-8).
* Call stack with maximum depth of 16 addresses.
* A beeper speaker.
* Keyboard with 16 keys in hexadecimal order (from 0x0 to 0xF).

## I want to study you code, what should I know to learn effectively?
Learning to re-create this emulator yourself is a great excessive for understanding computers, having theoretical over the matter will help you in long run.
With that, here are the following you need to know (at least theoretically) for best learning experience.

| Concept / Tool | Purpose |
| :---: | :---: |
| Von Neumann Machines | To understand computer architecture in general |
| Turing Machines | To understand how interpreters behave in general |
| Endianess | To understand how to decode hexadecimal instructions |
| Bitwise Operations | Will be useful in multiple areas like fetching opcodes, decoding address, register, etc. |

## What resources you used to create this emulator?
Here are some resources I referred while writing this emulator:
* CHIP-8 Technical Reference: http://devernay.free.fr/hacks/chip8/C8TECH10.HTM
* Tutorial used as reference (in C++): https://river.codes/emulating-a-computer-part-1/
* Micheal's code in C (to refer SDL integration): https://github.com/mk6502/chipee
