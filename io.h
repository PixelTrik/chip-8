#ifndef IO_H
#define IO_H

#include <stdbool.h>

typedef unsigned char byte;

typedef struct {
  unsigned char red, green, blue;
} color;

extern color fg, bg;
extern bool should_quit;

void init_io();

void draw(byte*);
void event_handler(byte*);
void audio_callback(void*, byte*, int);
void beep();

void stop_io();
#endif
